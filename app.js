const MongoClient = require("mongodb").MongoClient;
const assert = require("assert");
const express = require("express");
const mongoose = require("mongoose");
const querystring = require("querystring");
const { timestamp } = require("mongodb");
const PORT = 3000;
const app = express();
app.listen(PORT, () => {
  console.log(`Server listening on port: ${PORT}`);
  mongoose.connect("mongodb://localhost/klack", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  const db = mongoose.connection;
  console.log("connect to db");
  db.on("error", console.error.bind(console, "connection error:"));
  db.once("open", function () {
    //   // we're connected!
    // db.getTimestamp().toLocaleDateString(); //get the date
    // db.getTimestamp().toLocaleTimeString(); // get the time
    // var results = db.collection
    //   .aggregate([
    //     {
    //       $project: {
    //         ts: { $toDate: { $multiply: ["$unix_timestamp", 1000] } },
    //       },
    //     },
    //   ])
    //   .pretty();
  });

  const responseSchema = mongoose.Schema({
    timestamp: Number,
    sender: String,
    message: String,
  });
  const ResponseModel = mongoose.model("Response", responseSchema);

  app.use(express.urlencoded({ extended: false }));
  app.use(express.json());
  app.use(express.static("./public"));
  // app.use((req, res, next) => {
  //   res.locals.user = req.user;
  //   next();
  // });
  app.set("view engine", "pug");

  // List of all messages
  let messages = [];

  // Track last active times for each sender
  let users = {};

  // generic comparison function for case-insensitive alphabetic sorting on the name field
  function userSortFn(a, b) {
    var nameA = a.name.toUpperCase(); // ignore upper and lowercase
    var nameB = b.name.toUpperCase(); // ignore upper and lowercase
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }

    // names must be equal
    return 0;
  }

  app.get("/", (request, response) => {
    response.render("main", {
      getUserpane: function () {
        return request.user;
      },
    });
  });
  app.get("/messages", (request, response) => {
    const requireActiveSince = Date.now() - 15 * 1000;
    // db.users.find()._id.timestamp();

    usersSimple = Object.keys(users).map((x) => ({
      name: x,
      active: users[x] > requireActiveSince,
    }));

    // sort the list of users alphabetically by name
    usersSimple.sort(userSortFn);
    usersSimple.filter((a) => a.name !== request.query.for);

    // update the requesting user's last access time
    users[request.query.for] = Date.now();

    // send the latest 40 messages and the full user list, annotated with active flags
    ResponseModel.find(function (err, messages) {
      response.send({ messages: messages.slice(-40), users: usersSimple });
    });
  });
  app.post("/messages", (request, res) => {
    // add a timestamp to each incoming message.
    const timestamp = Date.now();
    request.body.timestamp = timestamp;
    const response = new ResponseModel(request.body);

    // append the new message to the message list
    messages.push(request.body);

    // update the posting user's last access timestamp (so we know they are active)
    users[request.body.sender] = timestamp;
    try {
      response.save(); //if else
      res.render("main");
    } catch (err) {
      res.send(err);
      res.statusCode = 201;
    }
  });
});
// });
